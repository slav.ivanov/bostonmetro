
public class Station implements iNode {
	
	private String stationName;
	private int id;
	
	public Station(String stationName, int id)
	{
		this.stationName = stationName;
		this.id = id;
	}

	@Override
	public String getName() {
		return stationName;
	}

	@Override
	public int getID() {
		return id;
	}

}
