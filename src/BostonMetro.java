import java.io.IOException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class BostonMetro {

    private iMultiGraph graph;

    public BostonMetro() {
        Parser parser;
        try {
            parser = new Parser("Stations.txt");
            parser.generateGraphFromFile();
            graph = parser.getGraph();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(0);
        } catch (BadFileException e) {
            e.printStackTrace();
        }
    }

    public void printPath(List<Line> constructedPath)
    {
        int pathLength = constructedPath.size()-1;
        if (constructedPath.size() == 0) {
            System.out.println("No path available");
            return;
        }
        String line = constructedPath.get(pathLength).getLabel();
        String changeinLine = line;
        System.out.println("Get the " + line + " line in direction: " + constructedPath.get(pathLength).getNode1().getName());
        for (int i = pathLength; i>=0; i--)
        {
            String station1 = "";
            String station2 = "";
            if (constructedPath.get(i).getNode1() != null)
                station1 = constructedPath.get(i).getNode1().getName();

            if (constructedPath.get(i).getNode2() != null)
                station2 = constructedPath.get(i).getNode2().getName();

            changeinLine = constructedPath.get(i).getLabel();

            if (!changeinLine.equals(line))
            {
                System.out.println("\n" + "Change to the " + constructedPath.get(i).getLabel() +
                        " line at"+ "\n" + station2 + " in direction: " + station1);
                line = changeinLine;
            }
            if (i == 0) {
                System.out.println("\nGet off at " + constructedPath.get(i).getNode1().getName());
            }

        }
    }

    private Station handleDuplicateStation(Station station) {

        //Deal with duplicate case
        Scanner sc = new Scanner(System.in);
        List<Station> stations = (List<Station>)(List<?>)graph.getNodesFromName(station.getName());

        //Hard codes since stations are unlikely to change
        System.out.println("1) St.Paulstreet id: 61 Line: GreenC");
        System.out.println("2) St.Paulstreet id: 38 Line: GreenB");
        System.out.println("Select which station you'd like to choose: ");
        int choice;
        try {
            choice = sc.nextInt();
            while (choice < 1 || choice > 2) {
                System.out.println("Choice must be a number that is presented");
                choice = sc.nextInt();
            }
            station = stations.get(choice - 1);
            return station;
        } catch (InputMismatchException e) {
            System.out.println("Choice must be an integer");
            return null;
        }
    }

    public static void main(String[] args) {

        BostonMetro bostonMetro = new BostonMetro();
        Scanner sc = new Scanner(System.in);
        String current = "";
        String destination = "";
        System.out.println("Welcome to the Boston Metro.");

        System.out.println("What is your current station?");
        current = sc.nextLine().replaceAll("\\s","");
        Station cur = (Station) bostonMetro.graph.getNode(current);
        while (cur==null)
        {
            System.out.println("Please type in a valid station");
            current = sc.nextLine().replaceAll("\\s+","");
            cur = (Station) bostonMetro.graph.getNode(current);
        }
        if (cur.getName().equalsIgnoreCase("St.PaulStreet")) {
            //Deal with duplicate case
            Station temp;
            while ((temp = bostonMetro.handleDuplicateStation(cur)) == null);
            cur = temp;
        }
        System.out.println("What is your destination?");
        destination = sc.nextLine().replaceAll("\\s+","");
        Station dest = (Station) bostonMetro.graph.getNode(destination);
        while (dest==null)
        {
            System.out.println("Please type in a valid station");
            destination = sc.nextLine().replaceAll("\\s+","");
            dest = (Station) bostonMetro.graph.getNode(destination);
        }
        if (dest.getName().equalsIgnoreCase("St.PaulStreet")) {
            //Deal with duplicate case
            Station temp;
            while ((temp = bostonMetro.handleDuplicateStation(dest)) == null);
            dest = temp;
        }
        if (dest.getID() == cur.getID()) {
            System.out.println("You're already at " + dest.getName());
        } else {
            //Cast to generic, then cast Line
            List<Line> bestRoute = (List<Line>)(List<?>) bostonMetro.graph.findPath(cur, dest);
            bostonMetro.printPath(bestRoute);
        }

        sc.close();
    }

}
