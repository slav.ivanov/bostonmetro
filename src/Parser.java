import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * This class reads a text description of a metro subway system
 * and generates a graph representation of the metro.
 * <p>
 * Students should feel free to modify this code as needed
 * to complete this exercise.
 * <p>
 * <p>
 * <p>
 * The grammar for the file is described below in BNF. A typical line
 * in the file looks like this :
 * <p>
 * <code> 20 NorthStation   Green 19 22  Orange 15 22  </code>
 * <p>
 * where :
 * 20 is the StationID
 * NorthStation is the StationName
 * Green 19 22
 * Green is the LineName
 * 19 is the StationID of the station2ID parsedStation
 * 22 is the StationID of the station1ID parsedStation
 * Orange 15 22 is a LineID in which :
 * Orange is the LineName
 * 15 is the StationID of the station2ID parsedStation
 * 22 is the StationID of the station1ID parsedStation
 * <p>
 * Therefore, NorthStation has two outgoing lines.
 * <p>
 * note : 0 denotes the end of a line : i.e. in this case,
 * OakGrove would be at the end of the line, as there is no other station2ID
 * parsedStation.
 * <p>
 * <p>
 * metro-map ::= parsedStation-spec* <BR>
 * parsedStation-spec ::= parsedStation-id parsedStation-name parsedStation-line+ <BR>
 * parsedStation-id ::= (positive integer) <BR>
 * parsedStation-name ::= string <BR>
 * parsedStation-line ::= line-name parsedStation-id parsedStation-id <BR>
 */

public class Parser {

    private BufferedReader fileInput;
    private iMultiGraph map;
    
    private static void usage() {
        //prints a usage message to System.out
        System.out.println("java ex3.MetroMapParser <filename>");
    }


    /**
     * @throws java.io.IOException if there <tt>filename</tt> cannot be read
     * @effects: creates a new parser that will read from the file
     * filename unless the file does not exist. The filename should specify
     * the exact location of the file. This means it should be something like
     * /mit/$USER/6.170/ex3/bostonmetro.txt
     * @returns a new MetroMapParser that will parse the file filename
     */

    public Parser(String filename) throws IOException {
        //a buffered reader reads line by line, returning null when file is done
        fileInput = new BufferedReader(new FileReader(filename));
        map = new MultiGraph();
    }

    /**
     * @effects: parses the file, and generates a graph from it, unless there
     * is a problem reading the file, or there is a problem with the format of the
     * file.
     * @throws java.io.IOException if there is a problem reading the file
     * @throws ex3.BadFileException if there is a problem with the format of the file
     * @returns the Graph generated by the file
     */

    //INTERNAL CLASS
    class tempLine {
        private String label;
        private int station1ID;
        private int station2ID;
        private Station parsedStation;

        public tempLine(String label, int station1ID, int station2ID, Station parsedStation) {
            this.label = label;
            this.station1ID = station1ID;
            this.station2ID = station2ID;
            this.parsedStation = parsedStation;
        }

        public String getLabel() {
            return label;
        }

        public int getStation1ID() {
            return station1ID;
        }

        public int getStation2ID() {
            return station2ID;
        }

        public Station getParsedStation() {
            return parsedStation;
        }
    }

    public void generateGraphFromFile() throws IOException, BadFileException {
        String line = fileInput.readLine();
        StringTokenizer st;
        String stationID;
        String stationName;
        String lineName;
        String outboundID, inboundID;


        Station station;

        tempLine templine = null;

        List<Station> stations = new ArrayList<>();
        List<tempLine> tempLines = new ArrayList<>();

        while (line != null) {

            //STUDENT :
            //
            //in this loop, you must collect the information necessary to
            //construct your graph, and you must construct your graph as well.
            //how and where you do this will depend on the design of your graph.
            //


            //StringTokenizer is a java.util Class that can break a string into tokens
            // based on a specified delimiter.  The default delimiter is " \t\n\r\f" which
            // corresponds to the space character, the tab character, the newline character,
            // the carriage-return character and the form-feed character.
            st = new StringTokenizer(line);

            //We want to handle empty lines effectively, we just ignore them!
            if (!st.hasMoreTokens()) {
                line = fileInput.readLine();
                continue;
            }

            //from the grammar, we know that the Station ID is the first token on the line
            stationID = st.nextToken();

            if (!st.hasMoreTokens()) {
                throw new BadFileException("no parsedStation name");
            }

            //from the grammar, we know that the Station Name is the second token on the line.
            stationName = st.nextToken();

            station = new Station(stationName, Integer.parseInt(stationID));
            map.addNode(station);
            stations.add(station);

            if (!st.hasMoreTokens()) {
                throw new BadFileException("parsedStation is on no lines");
            }


            while (st.hasMoreTokens()) {
                lineName = st.nextToken();

                if (!st.hasMoreTokens()) {
                    throw new BadFileException("poorly formatted line info");
                }

                outboundID = st.nextToken();

                if (!st.hasMoreTokens()) {
                    throw new BadFileException("poorly formatted adjacent stations");
                }
                inboundID = st.nextToken();

                templine = new tempLine(lineName, Integer.parseInt(outboundID), Integer.parseInt(inboundID), station);
                tempLines.add(templine);
            }
            line = fileInput.readLine();
        }

        //Create Line objects
        for (tempLine tempLine : tempLines) {
            Station station1 = findStation(tempLine.getStation1ID(), stations);
            Station station2 = findStation(tempLine.getStation2ID(), stations);
            //Create lines that are also reversed since directions don't matter
            //We could check here for end lines and avoid adding them if need be
            if (station1 != null) {
                Line newLine1 = new Line(tempLine.getLabel(), station1, tempLine.getParsedStation());
                Line newLine1Reversed = new Line(tempLine.getLabel(), tempLine.getParsedStation(), station1);
                map.addEdge(newLine1);
                map.addEdge(newLine1Reversed);
            }
            if (station2 != null) {
                Line newLine2 = new Line(tempLine.getLabel(), station2, tempLine.getParsedStation());
                Line newLine2Reversed = new Line(tempLine.getLabel(), tempLine.getParsedStation(), station2);
                map.addEdge(newLine2);
                map.addEdge(newLine2Reversed);
            }


        }
    }

    private Station findStation(int stationID, List<Station> stations) {
        for (Station station : stations) {
            if (station.getID() == stationID) {
                return station;
            }
        }
        return null;
    }
    
    public iMultiGraph getGraph() {
        return map;
    }
}