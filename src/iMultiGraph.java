import java.util.List;

public interface iMultiGraph {

    iNode getNode(String name);
    boolean addEdge(iEdge edge);
    boolean addNode(iNode node);
    List<iEdge> findPath(iNode sourceNode, iNode destinationNode);
    List<iNode> getNodesFromName(String name);

}
