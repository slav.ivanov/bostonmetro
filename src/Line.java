
public class Line implements iEdge {
	
	private iNode node1;
	private iNode node2;
	private String label;
	
	public Line(String label, iNode outNode, iNode inNode)
	{
		this.label = label;
		this.node2 = outNode;
		this.node1 = inNode;
	}

	public iNode getNode2() {
		return node2;
	}

	@Override
	public iNode getNode1() {
		return node1;
	}

	@Override
	public String getLabel() {
		return label;
	}
	

}
