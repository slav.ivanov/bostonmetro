public interface iEdge {
	
	iNode getNode1();

	iNode getNode2();
	
	String getLabel();	
}
