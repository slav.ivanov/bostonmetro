import java.util.*;

public class MultiGraph implements iMultiGraph {

	//Maps a node to its corresponding list of connected edges
	private Map<iNode, List<iEdge>> adjacencyListMap;

	public MultiGraph()
	{
		adjacencyListMap = new HashMap<>();
	}

    @Override
    public iNode getNode(String name) {
        for (Map.Entry<iNode, List<iEdge>> entry: adjacencyListMap.entrySet())
        {
            if (entry.getKey().getName().equalsIgnoreCase(name)) {
                return entry.getKey();
            }
        }
        //Not found
        return null;
    }

    @Override
	public boolean addEdge(iEdge edge) {
		//Don't need to check for duplicate edges since a multigraph allows this
		//Add edge to appropriate node adjacency list
        List<iEdge> nodeEdgeList1 = adjacencyListMap.get(edge.getNode1());
        if (nodeEdgeList1 != null)
            nodeEdgeList1.add(edge);
		return true;
	}

	@Override
	public boolean addNode(iNode node) {
	    //Check for duplicate node
        if (adjacencyListMap.containsKey(node)) {
            //Don't add node, duplicate
            return false;
        }
		adjacencyListMap.put(node, new ArrayList<>());
		return true;
	}

	private iEdge getEdge(iNode node1, iNode node2) {
	    List<iEdge> edges = adjacencyListMap.get(node1);
	    for (iEdge edge: edges) {
	        if  (edge.getNode1() == node1 && edge.getNode2() == node2) {
	            return edge;
            }
        }
        return null;
    }

	private List<iEdge> constructPath(Map<iNode, iNode> path, iNode destinationNode) {
	    //Keep grabbing the parent node till we get back to the start, we will then have a path
        List<iEdge> edges =  new ArrayList<>();
        iNode childNode = destinationNode;
        while (childNode != null) {
            iNode parentNode = path.get(childNode);
            iEdge edge = getEdge(childNode, parentNode);
            if (edge != null)
                edges.add(edge);
            childNode = parentNode;
        }
        return edges;
    }

    @Override
    public List<iEdge> findPath(iNode sourceNode, iNode destinationNode) {
        Deque<iNode> unvisitedNodes = new LinkedList<>();
        Set<iNode> visitedNodes = new HashSet<>();
        Map<iNode, iNode> path = new HashMap<>();
        unvisitedNodes.add(sourceNode);
        while (!unvisitedNodes.isEmpty()) {
            iNode subtreeRoot = unvisitedNodes.poll();
            //Check if current node is destination
            if (subtreeRoot == destinationNode) {
                return constructPath(path, destinationNode);
            }

            List<iNode> children = getNeighbours(subtreeRoot);
            for (iNode child : children) {
                //Ignore if same
                if (child == subtreeRoot)
                    continue;
                //Ignore visited
                if (visitedNodes.contains(child))
                    continue;

                if (!unvisitedNodes.contains(child)) {
                    unvisitedNodes.add(child);
                    path.put(child, subtreeRoot);
                }
            }
            visitedNodes.add(subtreeRoot);
        }
        return constructPath(path, destinationNode);
    }

    @Override
    public List<iNode> getNodesFromName(String name) {
	    List<iNode> nodes = new ArrayList<>();
        for (Map.Entry<iNode, List<iEdge>> entry: adjacencyListMap.entrySet())
        {
            if (entry.getKey().getName().equalsIgnoreCase(name)) {
                nodes.add(entry.getKey());
            }
        }
        return nodes;
    }

    private List<iNode> getNeighbours(iNode node) {
        List<iNode> neighbours = new ArrayList<>();
        List<iEdge> edges = adjacencyListMap.get(node);
        for (iEdge edge: edges) {
                if (edge.getNode2() != null) {
                    if (!neighbours.contains(edge.getNode2())) {
                        neighbours.add(edge.getNode2());
                    }
                }
        }
        return neighbours;
    }
	
}
